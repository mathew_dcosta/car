import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CarMakeComponent  } from '../app/car-make/car-make.component';

const routes: Routes = [
  { path: 'cars', component: CarMakeComponent},
  { path: '', redirectTo: 'cars',  pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
