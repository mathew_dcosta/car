import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

// Custom Imports
import { CarShow } from '../model/car.model';
import { environment } from '../../environments/environment';

import { CarMockService } from '../mock/car-mock';


@Injectable({
  providedIn: 'root'
})

export class CarMakeService {

  httpOptions = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json'
    })
  };

  // Get the baseUrl variable from environment
  baseUrl = environment.baseUrl;

  /**
  * Constructor
  */
  constructor(
    private http: HttpClient,
    private carMockService: CarMockService
  ) { }

  /**
   * Calls the car service to get random response
   */
  getCars(): any {
    // return this.http.get<CarShow[]>(`${this.baseUrl}/v1/cars`, this.httpOptions);
    return this.carMockService.getRandomResponse();
  }
}
