import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { CarMockService } from '../mock/car-mock';
import { CarMakeService } from './car-make.service';

describe('CarMakeService', () => {
  let injector: TestBed;
  let carMakeService: CarMakeService;
  let httpMock: HttpTestingController;
  let mockService: CarMockService;


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, HttpClientTestingModule],
      providers: [CarMakeService, CarMockService]
    });
    injector = getTestBed();
    carMakeService = injector.get(CarMakeService);
    httpMock = injector.get(HttpTestingController);
    mockService = injector.get(CarMockService);
  });

  it('should be created', () => {
    const service: CarMakeService = TestBed.get(CarMakeService);
    expect(service).toBeTruthy();
  });

  it('should return the response on api call', () => {
    carMakeService.getCars().subscribe((cars) => {
      expect(cars.length).toBeGreaterThanOrEqual(0);
      // expect(cars).toEqual(mockService.carSingleResponse);
    });
    // const httpRequest = httpMock.expectOne(`${carMakeService.baseUrl}/v1/cars`);
    // expect(httpRequest.request.method).toBe('GET');
    // httpRequest.flush(mockService.carSingleResponse);
    // httpMock.verify();
  });

});
