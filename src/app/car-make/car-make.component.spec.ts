import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';

import { CarMakeComponent } from './car-make.component';
import { CarMockService } from '../mock/car-mock';
import { CarMakeService } from '../services/car-make.service';
import { HttpClientModule } from '@angular/common/http';
import { of, throwError } from 'rxjs';
import { By } from '@angular/platform-browser';
import * as COMMON_CONSTANTS from '../common/common.constants';

describe('CarMakeComponent', () => {
  let component: CarMakeComponent;
  let fixture: ComponentFixture<CarMakeComponent>;
  let carService: CarMakeService;

  let mockService: CarMockService;
  let injector: TestBed;
  let debugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      declarations: [ CarMakeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarMakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    injector = getTestBed();
    mockService = injector.get(CarMockService);
    carService = fixture.debugElement.injector.get(CarMakeService);
    debugElement = fixture.debugElement;

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getCars and return a  list of cars', () => {
    spyOn(carService, 'getCars').and.returnValue(of(mockService.carResponse));
    component.getCarResponse();
    expect(component.cars).toEqual(mockService.carResponse);
  });

  it('should call getCars and return no cars', () => {
    spyOn(carService, 'getCars').and.returnValue(of(mockService.carEmptyResponse));
    component.getCarResponse();
    expect(component.cars.length).toBe(0);
  });

  it('should call getCars and error out', () => {
    spyOn(carService, 'getCars').and.returnValue(throwError({ status: 404 }));
    component.getCarResponse();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(debugElement.query(By.css('.error')).nativeElement.innerText)
        .toBe(COMMON_CONSTANTS.ERROR_MESSAGE);
    });
  });

  it('should call getCars and show warning when empty response is returned', () => {
    spyOn(carService, 'getCars').and.returnValue(of(mockService.carEmptyResponse));
    component.getCarResponse();
    expect(component.cars.length).toBe(0);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(debugElement.query(By.css('.warn')).nativeElement.innerText)
        .toBe(COMMON_CONSTANTS.ERROR_MESSAGE);
    });
  });

  it('should call getCars and display the model in the UI', () => {
    spyOn(carService, 'getCars').and.returnValue(of(mockService.carResponse));
    component.getCarResponse();
    expect(component.cars).toEqual(mockService.carResponse);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(debugElement.query(By.css('#model-0')).nativeElement.innerText).not
        .toBe('George 15 ');
    });
  });

  it('should call getCars and display the car make in the UI', () => {
    spyOn(carService, 'getCars').and.returnValue(of(mockService.carResponse));
    component.getCarResponse();
    expect(component.cars).toEqual(mockService.carResponse);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(debugElement.query(By.css('#car-0')).nativeElement.innerText).not
        .toBe('George Motors ');
    });
  });

  it('should call getCars and display the car show in the UI', () => {
    spyOn(carService, 'getCars').and.returnValue(of(mockService.carResponse));
    component.getCarResponse();
    expect(component.cars).toEqual(mockService.carResponse);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(debugElement.query(By.css('#show-0')).nativeElement.innerText).not
        .toBe('Melbourne Motor Show ');
    });
  });

  it('should call getCars and display the car make in the UI in ascending order', () => {
    spyOn(carService, 'getCars').and.returnValue(of(mockService.carResponse));
    component.getCarResponse();
    expect(component.cars).toEqual(mockService.carResponse);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(debugElement.query(By.css('#car-0')).nativeElement.innerText).not
        .not.toBe('Moto Tourismo ');
    });
  });
});
