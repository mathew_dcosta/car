import { Component, OnInit } from '@angular/core';
import { CarMakeService } from '../services/car-make.service';
import { CarShow, CarMake, CarModel, CarSample, CarObj } from '../model/car.model';
import * as _ from 'underscore';
import * as COMMON_CONSTANTS from '../common/common.constants';

@Component({
  selector: 'app-car-make',
  templateUrl: './car-make.component.html',
  styleUrls: ['./car-make.component.scss']
})
export class CarMakeComponent implements OnInit {

  cars: CarShow[];
  isError: boolean;
  carResult: Array<any> = [];
  finalCars: Array<any> = [];
  carSample: Array<CarObj> = [];
  finalCarsResult: any;

  errorMessage: string = COMMON_CONSTANTS.ERROR_MESSAGE;
  noDataFound: string = COMMON_CONSTANTS.NO_DATA;
  serverResponse: string;

  constructor(
    private carService: CarMakeService,
  ) { }

  /**
   * On init
   */
  ngOnInit() {
    this.getCarResponse();
  }

  // clear the objects used
  clearObjects() {
    this.cars = [];
    this.carSample = [];
    this.finalCars = [];
    this.carResult = [];
    this.isError = false;
  }

  /**
   * Call the getCars service
   */
  getCarResponse(): any {
    this.clearObjects();
    this.carService.getCars().subscribe(result => {
      this.serverResponse = result;
      if (result) {
        this.cars = result;
        this.formatResponse();
      }
    },
      (error) => {
        // change the error flag to show error message
        this.isError = true;
        this.serverResponse = error;
      });
  }


  formatResponse() {

    // flatten the object to hold the values in the same level
    this.carSample = this.cars;
    this.cars.forEach(show => {
      this.carSample.find(c => c.name === show.name).cars.map(m => m.name = show.name);
    });

    // move the make object and groupBy make
    this.carSample.forEach(sample => {
      this.carResult.push(
        _.chain(sample.cars)
          .groupBy('make')
          .map(function (models, make) {
            const removeMake =
              _.map(models, function (m) {
                return _.omit(m, 'make');
              });
            return {
              make: make,
              models: removeMake
            };
          })
          .value());
    });

    // flatten the arrays
    this.carResult = ([].concat.apply([], this.carResult));

    // sort by make
    this.finalCars.push(_.sortBy(this.carResult, 'make'));

    // remove the make from the outer object and have the make as key
    this.finalCarsResult = _.mapObject(_.groupBy(this.finalCars[0], 'make'),
      model => model.map(car => _.omit(car, 'make')));

  }
}
