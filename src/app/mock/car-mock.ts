import { Injectable } from '@angular/core';
import { CarShow } from '../model/car.model';
import { of, throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})

export class CarMockService {
    randomArray: Array<any> = [];
    count = 0;

    carResponse: Array<CarShow> = [
        {
            'name': 'Melbourne Motor Show',
            'cars': [
                {
                    'make': 'Julio Mechannica',
                    'model': 'Mark 4S'
                },
                {
                    'make': 'Hondaka',
                    'model': 'Elisa'
                },
                {
                    'make': 'Moto Tourismo',
                    'model': 'Cyclissimo'
                },
                {
                    'make': 'George Motors',
                    'model': 'George 15'
                },
                {
                    'make': 'Moto Tourismo',
                    'model': 'Delta 4'
                }
            ]
        },
        {
            'name': 'Cartopia',
            'cars': [
                {
                    'make': 'Moto Tourismo',
                    'model': 'Cyclissimo'
                },
                {
                    'make': 'George Motors',
                    'model': 'George 15'
                },
                {
                    'make': 'Hondaka',
                    'model': 'Ellen'
                },
                {
                    'make': 'Moto Tourismo',
                    'model': 'Delta 16'
                },
                {
                    'make': 'Moto Tourismo',
                    'model': 'Delta 4'
                },
                {
                    'make': 'Julio Mechannica',
                    'model': 'Mark 2'
                }
            ]
        }
    ];

    carSecondResponse: Array<CarShow> = [
        {
            'name': 'New York Car Show',
            'cars': [
                {
                    'make': 'Hondaka',
                    'model': 'Elisa'
                },
                {
                    'make': 'George Motors',
                    'model': 'George 15'
                },
                {
                    'make': 'Julio Mechannica',
                    'model': 'Mark 1'
                },
                {
                    'make': 'Moto Tourismo',
                    'model': 'Cyclissimo'
                },
                {
                    'make': 'Edison Motors',
                    'model': ''
                }
            ]
        },
        {
            'name': 'Melbourne Motor Show',
            'cars': [
                {
                    'make': 'Julio Mechannica',
                    'model': 'Mark 4S'
                },
                {
                    'make': 'Hondaka',
                    'model': 'Elisa'
                },
                {
                    'make': 'Moto Tourismo',
                    'model': 'Cyclissimo'
                },
                {
                    'make': 'George Motors',
                    'model': 'George 15'
                },
                {
                    'make': 'Moto Tourismo',
                    'model': 'Delta 4'
                }
            ]
        },
        {
            'name': 'Cartopia',
            'cars': [
                {
                    'make': 'Moto Tourismo',
                    'model': 'Cyclissimo'
                },
                {
                    'make': 'George Motors',
                    'model': 'George 15'
                },
                {
                    'make': 'Hondaka',
                    'model': 'Ellen'
                },
                {
                    'make': 'Moto Tourismo',
                    'model': 'Delta 16'
                },
                {
                    'make': 'Moto Tourismo',
                    'model': 'Delta 4'
                },
                {
                    'make': 'Julio Mechannica',
                    'model': 'Mark 2'
                }
            ]
        },
        {
            'name': 'Carographic',
            'cars': [
                {
                    'make': 'Hondaka',
                    'model': 'Elisa'
                },
                {
                    'make': 'Hondaka',
                    'model': 'Elisa'
                },
                {
                    'make': 'Julio Mechannica',
                    'model': 'Mark 4'
                },
                {
                    'make': 'Julio Mechannica',
                    'model': 'Mark 2'
                },
                {
                    'make': 'Moto Tourismo'
                },
                {
                    'make': 'Julio Mechannica',
                    'model': 'Mark 4'
                }
            ]
        },
        {
            'cars': [
                {
                    'make': 'Moto Tourismo',
                    'model': 'Delta 4'
                }
            ]
        }
    ];

    carSingleResponse: Array<CarShow> = [
        {
            'name': 'New York Car Show',
            'cars': [
                {
                    'make': 'Hondaka',
                    'model': 'Elisa'
                },
                {
                    'make': 'George Motors',
                    'model': 'George 15'
                },
                {
                    'make': 'Julio Mechannica',
                    'model': 'Mark 1'
                },
                {
                    'make': 'Moto Tourismo',
                    'model': 'Cyclissimo'
                },
                {
                    'make': 'Edison Motors',
                    'model': ''
                }
            ]
        }
    ];

    carEmptyResponse = '';

    constructor() { }

    getCarMock() {
        return of(this.carResponse);
    }

    getCarMockSecond() {
        return of(this.carSecondResponse);
    }

    getCarMockEmpty() {
        return of(this.carEmptyResponse);
    }

    getCarError() {
        return throwError(new HttpErrorResponse({ status: 400, statusText: 'Failed Downstream service' }));
    }

    getRandomResponse() {
        const randomArray = [this.getCarMock(), this.getCarMockSecond(), this.getCarError(), this.getCarMockEmpty()];
        const i = Math.floor(Math.random() * randomArray.length);

        return randomArray[i];
    }
}
