// Car
export interface CarShow {
    name?: string;
    cars: Car[];
}

// Make model
export interface Car {
    make: string;
    model?: string;
}


// CAR RESPONSE
export interface CarMake {
    make: string;
    models?: CarModel[];
}

export interface CarModel {
    model?: string;
    shows?: string[];
}

export interface CarObj {
    name?: string;
    cars: CarSample[];
}

// Make model
export interface CarSample {
    make?: string;
    name?: string;
    model?: string;
}

