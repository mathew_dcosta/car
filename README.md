# CarModel

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.1.

# Introduction

The project has a mocked response in the mock folder. The responses are generated randomly. Click on the Get Cars link to get the response. Each click will give a random response from the mocked data. 


Appropriate unit tests have also been included. The code coverage is 100%. 

To run the application, kindly clone the repository and run  `npm install` on the root directory.

## Development server

Run `ng run start` for a dev server. Navigate to `http://localhost:8080/`. The app will automatically reload if you change any of the source files.


## Running unit tests

Run `npn run test` to execute the unit tests via [Karma](https://karma-runner.github.io).


## Code Coverage

Run `npm run test --code-coverage` to get the code coverage report. 

